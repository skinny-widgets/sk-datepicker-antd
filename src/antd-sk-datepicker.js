
import { SkDatepickerImpl }  from '../../sk-datepicker/src/impl/sk-datepicker-impl.js';


import { DateFormat, DateParse, DateTime, DateLocale } from '../../dateutils/src/global.js';
import { SkRenderEvent } from "../../sk-core/src/event/sk-render-event.js";
import { CALENDAR_NO_ICON_AN } from "../../sk-datepicker/src/sk-datepicker.js";

export class AntdSkDatepicker extends SkDatepickerImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'datepicker';
    }

    get calendarPanel() {
        if (! this._calendarPanel) {
            this._calendarPanel = this.comp.el.querySelector('.ant-calendar-date-panel');
        }
        return this._calendarPanel;
    }

    set calendarPanel(el) {
        this._calendarPanel = el;
    }

    get input() {
        if (! this._input) {
            this._input = this.comp.el.querySelector('input.ant-input');
        }
        return this._input;
    }

    set input(el) {
        this._input = el;
    }

    get picker() {
        if (! this._picker) {
            this._picker = this.comp.el.querySelector('.ant-calendar-picker');
        }
        return this._picker;
    }

    set picker(el) {
        this._picker = el;
    }

    get pickerContainer() {
        if (! this._pickerContainer) {
            this._pickerContainer = this.comp.el.querySelector('.ant-calendar-picker-container');
        }
        return this._pickerContainer;
    }

    set pickerContainer(el) {
        this._pickerContainer = el;
    }

    get calendarTBody() {
        if (! this._calendarTBody) {
            this._calendarTBody = this.comp.el.querySelector('.ant-calendar-tbody');
        }
        return this._calendarTBody;
    }

    set calendarTBody(el) {
        this._calendarTBody = el;
    }

    get calendarTHead() {
        if (! this._calendarTHead) {
            this._calendarTHead = this.comp.el.querySelector('.ant-calendar-table thead');
        }
        return this._calendarTHead;
    }

    set calendarTHead(el) {
        this._calendarTHead = el;
    }

    get curMonthLabel() {
        if (! this._curMonthLabel) {
            this._curMonthLabel = this.comp.el.querySelector('.ant-calendar-month-select');
        }
        return this._curMonthLabel;
    }

    set curMonthLabel(el) {
        this._curMonthLabel = el;
    }

    get curYearLabel() {
        if (! this._curYearLabel) {
            this._curYearLabel = this.comp.el.querySelector('.ant-calendar-year-select');
        }
        return this._curYearLabel;
    }

    set curYearLabel(el) {
        this._curYearLabel = el;
    }

    get prevYearBtn() {
        if (! this._prevYearBtn) {
            this._prevYearBtn = this.comp.el.querySelector('.ant-calendar-prev-year-btn');
        }
        return this._prevYearBtn;
    }

    set prevYearBtn(el) {
        this._prevYearBtn = el;
    }

    get prevMonthBtn() {
        if (! this._prevMonthBtn) {
            this._prevMonthBtn = this.comp.el.querySelector('.ant-calendar-prev-month-btn');
        }
        return this._prevMonthBtn;
    }

    set prevMonthBtn(el) {
        this._prevMonthBtn = el;
    }

    get selectMonthBtn() {
        if (! this._selectMonthBtn) {
            this._selectMonthBtn = this.comp.el.querySelector('.ant-calendar-month-select');
        }
        return this._selectMonthBtn;
    }

    set selectMonthBtn(el) {
        this._selectMonthBtn = el;
    }

    get selectYearBtn() {
        if (! this._selectYearBtn) {
            this._selectYearBtn = this.comp.el.querySelector('.ant-calendar-year-select');
        }
        return this._selectYearBtn;
    }

    set selectYearBtn(el) {
        this._selectYearBtn = el;
    }

    get nextYearBtn() {
        if (! this._nextYearBtn) {
            this._nextYearBtn = this.comp.el.querySelector('.ant-calendar-next-year-btn');
        }
        return this._nextYearBtn;
    }

    set nextYearBtn(el) {
        this._nextYearBtn = el;
    }

    get nextMonthBtn() {
        if (! this._nextMonthBtn) {
            this._nextMonthBtn = this.comp.el.querySelector('.ant-calendar-next-month-btn');
        }
        return this._nextMonthBtn;
    }

    set nextMonthBtn(el) {
        this._nextMonthBtn = el;
    }

    get todayBtn() {
        if (! this._todayBtn) {
            this._todayBtn = this.comp.el.querySelector('.ant-calendar-today-btn');
        }
        return this._todayBtn;
    }

    set todayBtn(el) {
        this._todayBtn = el;
    }

    get calendarInput() {
        if (! this._calendarInput) {
            this._calendarInput = this.comp.el.querySelector('.ant-calendar-input');
        }
        return this._calendarInput;
    }

    set calendarInput(el) {
        this._calendarInput = el;
    }

    get calendarPickerInput() {
        if (! this._calendarPickerInput) {
            this._calendarPickerInput = this.comp.el.querySelector('.ant-calendar-picker-input');
        }
        return this._calendarPickerInput;
    }

    set calendarPickerInput(el) {
        this._calendarPickerInput = el;
    }

    get subEls() {
        return [ 'calendarPanel', 'input', 'picker', 'pickerContainer', 'calendarTBody', 'curMonthLabel', 'calendarTHead',
            'curMonthLabel', 'curYearLabel', 'prevYearBtn', 'prevMonthBtn', 'selectMonthBtn', 'selectYearBtn', 'nextYearBtn',
            'nextMonthBtn', 'todayBtn', 'calendarInput', 'calendarPickerInput' ];
    }

    get currentYear() {
        return this._currentYear || DateTime.today().getFullYear();
    }

    set currentYear(newYear) {
        this._currentYear = newYear;
    }

    get currentMonth() {
        return this._currentMonth || DateTime.today().getMonth();
    }

    set currentMonth(newMonth) {
        this._currentMonth = newMonth;

    }

    doRender() {
        let id = this.getOrGenId();
        this.beforeRendered();
        this.renderWithVars(id);
        this.indexMountedStyles();
        if (this.comp.value) {
            this.updateInputValue();
        }
        // :TODO preset to filled in input value
        let now = DateTime.today();
        this.currentYear = now.getFullYear();
        this.currentMonth = now.getMonth();

        this.renderPickerLabel();

        this.renderWeekDaysHeading();

        this.renderDays();
        if (this.comp.dateLocale === DateLocale.RU) {
            this.todayBtn.innerHTML = 'Сегодня';
        }
        this.afterRendered();
        this.bindEvents();
        this.comp.bindAutoRender();
        this.comp.setupConnections();
        this.comp.callPluginHook('onRenderEnd');
        this.comp.implRenderTimest = Date.now();
        this.comp.removeFromRendering();
        this.comp.dispatchEvent(new CustomEvent('rendered', { bubbles: true, composed: true })); // :DEPRECATED
        this.comp.dispatchEvent(new SkRenderEvent({ bubbles: true, composed: true }));
        if (this.comp.renderDeferred) {
            this.comp.renderDeferred.resolve(this.comp);
        }
    }

    renderImpl() {
        let themeLoaded = this.initTheme();
        themeLoaded.finally(() => { // we just need to try loading styles
            this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template
            if (!this.comp.tpl) {
                const loadTpl = async () => {
                    this.comp.tpl = await this.comp.renderer.mountTemplate(this.tplPath, this.cachedTplId,
                        this.comp, {
                            themePath: this.themePath
                        });
                };
                loadTpl().then(() => {
                    this.doRender();
                });
            } else {
                this.doRender();
            }
        });


    }

    updateInputValue() {
        this.calendarInput.value = this.comp.value;
        this.calendarPickerInput.value = this.comp.value;
        this.comp.dispatchEvent(new CustomEvent('input', { target: this.comp, bubbles: true, composite: true, detail: { }}));
        this.comp.dispatchEvent(new CustomEvent('skinput', { target: this.comp, bubbles: true, composite: true, detail: { }}));
    }

    renderPickerLabel() {
        this.curMonthLabel.innerHTML = DateFormat.format(DateTime.fromDate(this.currentYear, this.currentMonth, 1), 'F', this.comp.dateLocale);
        this.curMonthLabel.setAttribute('ref', this.currentMonth);
        this.curYearLabel.innerHTML = this.currentYear;
    }

    renderDays() {
        this.calendarTBody.innerHTML = '';
        let firstMonthDay = DateTime.fromDate(this.currentYear, this.currentMonth, 1);
        let firstWeekDay = firstMonthDay.getFirstDateOfWeek(this.comp.dateLocale);

        for (let w = 0; w <= 5; w++) { // weeks as displayed in picker
            let weekRow = this.comp.renderer.createEl('tr');
            weekRow.setAttribute('role', 'row');
            for (let i = 1; i <= 7; i++) {
                let dayOffset = ((w * 7) + i) - 1;
                let day = firstWeekDay.plusDays(dayOffset);
                let dayCell =
                    `<td role="gridcell" data-year="${day.getFullYear()}" data-month="${day.getMonth()}" data-day="${day.getDate()}" title="${DateFormat.format(day, 'd F Y', this.comp.dateLocale)}" class="ant-calendar-cell">
                        <div class="ant-calendar-date" aria-selected="false" aria-disabled="false">${DateFormat.format(day, 'j', this.comp.dateLocale)}</div>
                    </td>`;
                weekRow.insertAdjacentHTML('beforeend', dayCell);
            }
            this.calendarTBody.appendChild(weekRow);
        }
    }

    renderWeekDaysHeading() {
        let wdRow = this.comp.renderer.createEl('tr');
        wdRow.setAttribute('role', 'row');
        let firstWdDay = DateTime.today().getFirstDateOfWeek(this.comp.dateLocale);
        for (let wd = 0; wd <= 6; wd++) { // weekday code heading
            let weekDay = firstWdDay.plusDays(wd);
            let wdCell =
                `<th role="columnheader" title="${DateFormat.format(weekDay, 'D', this.comp.dateLocale)}" class="ant-calendar-column-header">
                    <span class="ant-calendar-column-header-inner">${DateFormat.format(weekDay, 'D', this.comp.dateLocale)}</span>
                </th>`
            ;
            wdRow.insertAdjacentHTML('beforeend', wdCell);
        }
        this.calendarTHead.appendChild(wdRow);
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState();
        this.clearAllElCache();
        this.pickerContainer.style.display = 'none';
        this.picker.addEventListener('focusin', (event) => {
            this.showPicker();
        });
        this.pickerContainer.addEventListener('focusin', (event) => {
            this.showPicker();
        });
        this.pickerContainer.addEventListener('focusout', (event) => {
            this.closePicker();
        });
        this.renderPickerIcon();
        this.mountStyles();
    }
    
    renderPickerIcon() {
        if (! this.comp.hasAttribute(CALENDAR_NO_ICON_AN)) {
            this.input.insertAdjacentHTML('afterend',
                `<i aria-label="icon: calendar" class="anticon anticon-calendar ant-calendar-picker-icon">
                    <svg
                        viewBox="64 64 896 896" focusable="false" class="" data-icon="calendar" width="1em" height="1em"
                        fill="currentColor" aria-hidden="true">
                        <path
                            d="M880 184H712v-64c0-4.4-3.6-8-8-8h-56c-4.4 0-8 3.6-8 8v64H384v-64c0-4.4-3.6-8-8-8h-56c-4.4 0-8 3.6-8 8v64H144c-17.7 0-32 14.3-32 32v664c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V216c0-17.7-14.3-32-32-32zm-40 656H184V460h656v380zM184 392V256h128v48c0 4.4 3.6 8 8 8h56c4.4 0 8-3.6 8-8v-48h256v48c0 4.4 3.6 8 8 8h56c4.4 0 8-3.6 8-8v-48h128v136H184z"></path>
                    </svg>
                </i>`);
        }
    }
    
    closePicker() {
        this.pickerContainer.style.display = 'none';
        this.picker.style.display = 'inline-block';
        this.comp.removeAttribute('open');
    }

    showPicker() {
        this.picker.style.display = 'none';
        this.pickerContainer.style.display = 'inline-block';
        this.comp.setAttribute('open', '');
    }

    selectDate(dateData) {
        let year = parseInt(dateData['year']);
        let month = parseInt(dateData['month']);
        let day = parseInt(dateData['day']);
        let date = DateTime.fromDate(year, month, day);
        this.comp.value = DateFormat.format(date, this.comp.fmt, this.comp.dateLocale);
        this.updateInputValue();
    }

    selectDay(dayValue) {
        let day = DateTime.fromDate(this.currentYear, this.currentMonth, dayValue);
        this.comp.value = DateFormat.format(day, this.comp.fmt, this.dateLocale);
        this.updateInputValue();
    }

    clearValue() {
        this.comp.value = '';
        this.updateInputValue();
    }

    bindEvents() {
        super.bindEvents();
        this.comp.el.addEventListener('click', (event) => {
            let targetEl = event.target;
            let currentDate = DateTime.fromDate(this.currentYear, this.currentMonth, 1);
            if (targetEl) {
                if (targetEl.classList.contains('ant-calendar-date')) {
                    this.selectDate(targetEl.parentElement.dataset);
                    this.closePicker();
                } else if (targetEl.classList.contains('ant-input-clear')) {
                    this.clearValue();
                    this.closePicker();
                } else if (targetEl === this.selectMonthBtn) {
                    this.currentMonth = parseInt(event.target.getAttribute('ref'));
                    let date = DateParse.parseDate(this.input.value, this.comp.fmt);
                    this.selectDay(date.getDay());
                    this.closePicker();
                } else if (targetEl === this.selectYearBtn) {
                    this.currentYear = parseInt(event.target.innerText);
                    let date = DateParse.parseDate(this.input.value, this.comp.fmt);
                    this.selectDay(date.getDay());
                    this.closePicker();
                } else if (targetEl === this.prevMonthBtn) {
                    if (this.currentMonth > 1 && this.currentMonth <= 12) {
                        currentDate = DateTime.fromDate(this.currentYear, this.currentMonth - 1, 1);
                    } else {
                        if (this.currentMonth <= 1) {
                            currentDate = DateTime.fromDate(this.currentYear - 1, 12, 1);
                        }
                    }
                } else if (targetEl === this.nextMonthBtn) {
                    if (this.currentMonth >= 1 && this.currentMonth < 12) {
                        currentDate = DateTime.fromDate(this.currentYear, this.currentMonth + 1, 1);
                    } else {
                        if (this.currentMonth >= 12) {
                            currentDate = DateTime.fromDate(this.currentYear + 1, 1, 1);
                        }
                    }
                } else if (targetEl === this.prevYearBtn) {
                    currentDate = DateTime.fromDate(this.currentYear - 1, this.currentMonth, 1);
                } else if (targetEl === this.nextYearBtn) {
                    currentDate = DateTime.fromDate(this.currentYear + 1, this.currentMonth, 1);
                } else if (targetEl === this.todayBtn) {
                    let now = DateTime.today();
                    this.currentYear = now.getFullYear();
                    this.currentMonth = now.getMonth();
                    this.selectDay(now.getDate());
                    this.closePicker();
                } else if (targetEl.tagName === 'path' || targetEl.tagName === 'svg') {
                    if (this.comp.hasAttribute('open')) {
                        this.closePicker();
                    } else {
                        this.showPicker();
                    }
                }
                this.currentYear = currentDate.getFullYear();
                this.currentMonth = currentDate.getMonth();
                this.renderPickerLabel();
                this.renderDays();
            }
        })
    }

}
